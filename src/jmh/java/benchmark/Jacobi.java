	package benchmark;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class Jacobi {

	private final ExecutorService srv;
	private final int matrix_size;
	private final int block_size;
	private final int niter;
	private final boolean check;

	public Jacobi(ExecutorService srv, int matrix_size, int block_size, int niter, boolean check) {
		this.srv = srv;
		this.matrix_size = matrix_size;
		this.block_size = block_size;
		this.niter = niter;
		this.check = check;
	}

	private static void copy_block(int nx, int ny, int block_x, int block_y,
			double u[][], double unew[][], int block_size) {
		int i, j, start_i, start_j;
		start_i = block_x * block_size;
		start_j = block_y * block_size;
		for (i = start_i; i < start_i + block_size; i++) {
			for (j = start_j; j < start_j + block_size; j++) {
				u[i][j] = unew[i][j];
			}
		}
	}

	private static void compute_estimate(int block_x, int block_y,
			double u[][], double unew[][], double f[][], double dx, double dy,
			int nx, int ny, int block_size) {
		int i, j, start_i, start_j;
		start_i = block_x * block_size;
		start_j = block_y * block_size;
		for (i = start_i; i < start_i + block_size; i++) {
			for (j = start_j; j < start_j + block_size; j++) {
				if (i == 0 || j == 0 || i == nx - 1 || j == ny - 1) {
					unew[i][j] = f[i][j];
				} else {
					unew[i][j] = 0.25 * (u[i - 1][j] + u[i][j + 1]
							+ u[i][j - 1] + u[i + 1][j] + f[i][j] * dx * dy);
				}
			}
		}
	}

	private void sweep(int nx, int ny, double dx, double dy, double f[][],
			int itold, int itnew, double u[][], double unew[][], int block_size)
			throws ExecutionException, InterruptedException {
		int it;
		int block_x, block_y;

		if (block_size == 0)
			block_size = nx;

		final int max_blocks_x = (nx / block_size);
		final int max_blocks_y = (ny / block_size);

		Future<?>[][][] copyF = new Future[itnew - itold][max_blocks_x][max_blocks_y];
		Future<?>[][][] estF = new Future[itnew - itold][max_blocks_x][max_blocks_y];

		for (it = itold + 1; it <= itnew; it++) {
			// Save the current estimate.
			for (block_x = 0; block_x < max_blocks_x; block_x++) {
				for (block_y = 0; block_y < max_blocks_y; block_y++) {
					final int bx = block_x;
					final int by = block_y;
					final int cur_it = it;
					final int old_it = itold;
					final int bs = block_size;
					Future<?> cpF = srv
							.submit(() -> {
								try {
									if (cur_it != old_it + 1) {
										estF[cur_it - old_it - 2][bx][by].get();
										if (bx != 0)
											estF[cur_it - old_it - 2][bx - 1][by]
													.get();
										if (bx != max_blocks_x - 1)
											estF[cur_it - old_it - 2][bx + 1][by]
													.get();
										if (by != 0)
											estF[cur_it - old_it - 2][bx][by - 1]
													.get();
										if (by != max_blocks_y - 1)
											estF[cur_it - old_it - 2][bx][by + 1]
													.get();
									}
								} catch (Exception e) {
									throw new RuntimeException(e);
								}
								copy_block(nx, ny, bx, by, u, unew, bs);
							});
					copyF[it - itold - 1][block_x][block_y] = cpF;
				}
			}
			// Compute a new estimate.
			for (block_x = 0; block_x < max_blocks_x; block_x++) {
				for (block_y = 0; block_y < max_blocks_y; block_y++) {
					final int bx = block_x;
					final int by = block_y;
					final int cur_it = it;
					final int old_it = itold;
					final int bs = block_size;
					Future<?> eF = srv
							.submit(() -> {
								try {
									copyF[cur_it - old_it - 1][bx][by].get();
									if (bx != 0)
										copyF[cur_it - old_it - 1][bx - 1][by]
												.get();
									if (bx != max_blocks_x - 1)
										copyF[cur_it - old_it - 1][bx + 1][by]
												.get();
									if (by != 0)
										copyF[cur_it - old_it - 1][bx][by - 1]
												.get();
									if (by != max_blocks_y - 1)
										copyF[cur_it - old_it - 1][bx][by + 1]
												.get();
								} catch (Exception e) {
								}
								compute_estimate(bx, by, u, unew, f, dx, dy,
										nx, ny, bs);
							});
					estF[it - itold - 1][block_x][block_y] = eF;
				}
			}
		}
	}

	private static void sweep_seq(int nx, int ny, double dx, double dy,
			double f[][], int itold, int itnew, double u[][], double unew[][]) {
		int i;
		int it;
		int j;

		for (it = itold + 1; it <= itnew; it++) {
			for (i = 0; i < nx; i++) {
				for (j = 0; j < ny; j++) {
					u[i][j] = unew[i][j];
				}
			}
			for (i = 0; i < nx; i++) {
				for (j = 0; j < ny; j++) {
					if (i == 0 || j == 0 || i == nx - 1 || j == ny - 1) {
						unew[i][j] = f[i][j];
					} else {
						unew[i][j] = 0.25 * (u[i - 1][j] + u[i][j + 1]
								+ u[i][j - 1] + u[i + 1][j] + f[i][j] * dx * dy);
					}
				}
			}
		}
	}

	/* Evaluates the exact solution. */
	private static double u_exact(double x, double y) {
		double pi = 3.141592653589793;
		double value;

		value = Math.sin(pi * x * y);

		return value;
	}

	/* Evaluates (d/dx d/dx + d/dy d/dy) of the exact solution. */
	private static double uxxyy_exact(double x, double y) {
		double pi = 3.141592653589793;
		double value;

		value = -pi * pi * (x * x + y * y) * Math.sin(pi * x * y);

		return value;
	}

	private static double r8mat_rms(int nx, int ny, double a[][]) {
		int i;
		int j;
		double v;

		v = 0.0;

		for (j = 0; j < ny; j++) {
			for (i = 0; i < nx; i++) {
				v = v + a[i][j] * a[i][j];
			}
		}
		v = Math.sqrt(v / (double) (nx * ny));

		return v;
	}

	private static void rhs(int nx, int ny, double f[][], int block_size) {
		int i, ii;
		int j, jj;
		double x;
		double y;

		for (j = 0; j < ny; j += block_size)
			for (i = 0; i < nx; i += block_size)
				for (jj = j; jj < j + block_size; ++jj) {
					y = (double) (jj) / (double) (ny - 1);
					for (ii = i; ii < i + block_size; ++ii) {
						x = (double) (ii) / (double) (nx - 1);
						if (ii == 0 || ii == nx - 1 || jj == 0 || jj == ny - 1)
							f[ii][jj] = u_exact(x, y);
						else
							f[ii][jj] = -uxxyy_exact(x, y);
					}
				}
	}

	public void run() throws ExecutionException, InterruptedException {
		int nx = matrix_size;
		int ny = matrix_size;

		double dx = 1.0 / (double) (nx - 1);
		double dy = 1.0 / (double) (ny - 1);

		double f[][] = new double[nx][ny];
		double u[][] = new double[nx][ny];
		double unew[][] = new double[nx][ny];

		int ii, i;
		int jj, j;

		// Set the right hand side array F.
		rhs(nx, ny, f, block_size);

		for (j = 0; j < ny; j += block_size)
			for (i = 0; i < nx; i += block_size)
				for (jj = j; jj < j + block_size; ++jj)
					for (ii = i; ii < i + block_size; ++ii) {
						if (ii == 0 || ii == nx - 1 || jj == 0 || jj == ny - 1) {
							unew[ii][jj] = f[ii][jj];
						} else {
							unew[ii][jj] = 0.0;
						}
					}

		// sweep_seq(nx, ny, dx, dy, f, 0, niter, u, unew);
		sweep(nx, ny, dx, dy, f, 0, niter, u, unew, block_size);
		if (!check(nx, ny, dx, dy, f, u, unew)) {
			throw new IllegalStateException("Benchmark has a bug!");
		}
	}

	private boolean check(int nx, int ny, double dx, double dy, double[][] f,
			double[][] u, double[][] unew) {
		if (!check) {
			return true;
		}
		double error;
		int i;
		int j;
		double x;
		double y;
		double udiff[][] = new double[nx][ny];
		for (j = 0; j < ny; j++) {
			y = (double) (j) / (double) (ny - 1);
			for (i = 0; i < nx; i++) {
				x = (double) (i) / (double) (nx - 1);
				udiff[i][j] = unew[i][j] - u_exact(x, y);
			}
		}
		error = r8mat_rms(nx, ny, udiff);

		double error1;
		// Set the right hand side array F.
		rhs(nx, ny, f, block_size);

		/*
		 * Set the initial solution estimate UNEW. We are "allowed" to pick up
		 * the boundary conditions exactly.
		 */
		for (j = 0; j < ny; j++) {
			for (i = 0; i < nx; i++) {
				if (i == 0 || i == nx - 1 || j == 0 || j == ny - 1) {
					unew[i][j] = f[i][j];
				} else {
					unew[i][j] = 0.0;
				}
			}
		}

		sweep_seq(nx, ny, dx, dy, f, 0, niter, u, unew);

		// Check for convergence.
		for (j = 0; j < ny; j++) {
			y = (double) (j) / (double) (ny - 1);
			for (i = 0; i < nx; i++) {
				x = (double) (i) / (double) (nx - 1);
				udiff[i][j] = unew[i][j] - u_exact(x, y);
			}
		}
		error1 = r8mat_rms(nx, ny, udiff);
		return Math.abs(error - error1) < 1.0E-6;
	}

	public static void jacobi(ExecutorService srv, int matrix_size, int block_size, int niter,
			boolean check) throws ExecutionException, InterruptedException {
		Jacobi run = new Jacobi(srv, matrix_size, block_size, niter, check);
		run.run();
	}
}
