package benchmark;

import gorn.Defaults;
import gorn.Defaults.Flag;
import gorn.java.ext.Executors;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;

@State(Scope.Benchmark)
public class JacobiBenchmark {

	int matrix_size = 4* 1024;
	int block_size = 512;
	int niter = 30;
	boolean check = false;
	ExecutorService srv;
	

    /*
     * And, check the benchmark went fine afterwards:
     */

    @TearDown
    public void cleanUp() {
    	srv.shutdown();
    }
    
	void jacobi(ExecutorService srv) {
		this.srv = srv;
		try {
			Jacobi.jacobi(srv, matrix_size, block_size, niter, check);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Benchmark
    @BenchmarkMode(Mode.SingleShotTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
	public void defaults() {
		jacobi(Executors.newCachedThreadPool());
	}

	@Benchmark
    @BenchmarkMode(Mode.SingleShotTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
	public void snapshot() {
		Defaults.resetFlags();
		Defaults.setFlag(Flag.USE_KNOWN_SET_SNAPSHOTS, true);
		Defaults.setFlag(Flag.USE_KNOWN_SET_CACHE, false);
		jacobi(Executors.newCachedThreadPool());
	}

	@Benchmark
    @BenchmarkMode(Mode.SingleShotTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
	public void layered() {
		Defaults.resetFlags();
		Defaults.setFlag(Flag.USE_KNOWN_SET_LAYERED, true);
		Defaults.setFlag(Flag.USE_KNOWN_SET_CACHE, false);
		jacobi(Executors.newCachedThreadPool());
	}

	@Benchmark
    @BenchmarkMode(Mode.SingleShotTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
	public void baseline() {
		jacobi(java.util.concurrent.Executors.newCachedThreadPool());
	}
}
