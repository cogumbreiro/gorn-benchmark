package benchmark;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import gorn.sif.BasicKnownSet;
import gorn.sif.CachedKnownSet;
import gorn.sif.KnownSet;
import gorn.sif.LayeredKnownSet;
import gorn.sif.SnapKnownSet;
import gorn.vc.VCKnownSet;

import java.util.HashMap;
import java.util.Map;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

@State(Scope.Benchmark)
public class KnownSetBenchmark {


	private static void run(KnownSet<Integer> set, int childrenCount) {
		HashMap<Integer, KnownSet<Integer>> copies = new HashMap<>();
		int i;
		@SuppressWarnings("unchecked")
		KnownSet<Integer>[] children = new KnownSet[childrenCount];
		for (i = 0; i < childrenCount; i++) {
			int task = i * 10;
			children[i] = set.clone();
			children[i].setTask(task);
			copies.put(task, children[i]);
			set.fork(task);
			assertTrue("set.isSafeToJoin(" + task + ")",  set.tryJoin(task, children[i]));
		}
		int childId = 0;
		for (@SuppressWarnings("unused") KnownSet<Integer> child : children) {
			for (i = 1; i < childrenCount; i++) {
				int id = childId * 10 + i;
				KnownSet<Integer> ks = children[i].clone();
				copies.put(id, ks);
				ks.setTask(id);
				children[i].fork(id);
				assertTrue(children[i].tryJoin(id, copies.get(id)));
			}
			childId++;
		}
		i = 0;
		for (KnownSet<Integer> child : children) {
			assertTrue("set.isSafeToJoin(" +  (i * 10) + ")",  set.tryJoin(i * 10, child));
			i++;
		}
		childId = 0;
		for (@SuppressWarnings("unused") KnownSet<Integer> child : children) {
			for (i = 1; i < childrenCount; i++) {
				assertTrue(set.tryJoin(i * 10, copies.get(i*10)));
			}
			childId++;
		}
	}

	private static void runMany(KnownSet<Integer> set, int count) {
		Map<Integer, KnownSet<Integer>> copies = new HashMap<>();
		for (int i = 0; i < count; i++) {
			KnownSet<Integer> ks = set.clone();
			copies.put(i, ks);
			set.fork(i);
			ks.setTask(i);
		}
		for (int i = 0; i < count; i++) {
			assertTrue("set.isSafeToJoin("+ i + ")", set.tryJoin(i, copies.get(i)));
		}
		for (int i = count; i < count + 100; i++) {
			assertFalse("NOT set.isSafeToJoin("+ i + ")", set.tryJoin(i, copies.get(i)));
		}
	}
	
    @Benchmark
    public void A_cached() {
    	run(new CachedKnownSet<Integer>(100), 100);
    }
	
    @Benchmark
    public void A_snap() {
    	run(new SnapKnownSet<Integer>(), 100);
    }

    @Benchmark
    public void A_vectorclock() {
    	run(new VCKnownSet<Integer>(), 100);
    }

    private static final int B_SIZE = 500;

    @Benchmark
    public void B_cached() {
    	runMany(new CachedKnownSet<Integer>(100), B_SIZE);
    }

    @Benchmark
    public void B_layered() {
    	runMany(new LayeredKnownSet<Integer>(), B_SIZE);
    }
	
    @Benchmark
    public void B_vectorclock() {
    	runMany(new VCKnownSet<Integer>(), B_SIZE);
    }
	
    @Benchmark
    public void B_snap() {
    	runMany(new SnapKnownSet<Integer>(), B_SIZE);
    }

    @Benchmark
    public void B_basic() {
    	runMany(new BasicKnownSet<Integer>(), B_SIZE);
    }
}
