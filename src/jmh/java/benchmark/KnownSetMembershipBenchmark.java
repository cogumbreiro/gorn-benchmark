package benchmark;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import benchmark.knownset.Harness;
import benchmark.knownset.Task;

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
public class KnownSetMembershipBenchmark {

	Harness d;
	
	@Setup
	public void init() {
		d = Harness.LARGE;
	}
/*
    @Benchmark
    public void isMemberCached() {
    	for (Task memberTask : d.memberTasks) {
    		if (!d.sourceCached.isSafeToJoin(memberTask)) throw new IllegalStateException();
    	}
    }

    @Benchmark
    public void isMemberUncached() {
    	for (Task memberTask : d.memberTasks) {
    		if (!d.sourceUncached.isSafeToJoin(memberTask)) throw new IllegalStateException();
    	}
    }

    @Benchmark
    public void isMemberSnap() {
    	for (Task memberTask : d.memberTasks) {
    		if (!d.sourceSnap.isSafeToJoin(memberTask)) throw new IllegalStateException();
    	}
    }


    @Benchmark
    public void notMemberCached() {
    	d.sourceCached.isSafeToJoin(d.targetTask);
    }

    @Benchmark
    public void notMemberUncached() {
    	d.sourceUncached.isSafeToJoin(d.targetTask);
    }
*/
}
