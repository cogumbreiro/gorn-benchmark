package benchmark;

import java.util.concurrent.TimeUnit;

import gorn.sif.snapshotset.Snapshot;
import gorn.sif.snapshotset.SnapshotArrayList;
import gorn.sif.snapshotset.SnapshotLinkedList;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class SnapshotListAddBenchmark {

	SnapshotLinkedList<Integer> list1;
	SnapshotArrayList<Integer> list2;
	

    @Setup(Level.Iteration)
    public void setup(){
        list1 = new SnapshotLinkedList<>();
        list2 = new SnapshotArrayList<>();
    }
    
    public static final int COUNT = 500000;
    public static final int ITERS = 30;
    public static final int WARMUP= 5;
    
    @Benchmark
    @Warmup(iterations = WARMUP, batchSize = COUNT)
    @Measurement(iterations = ITERS, batchSize = COUNT)
    @BenchmarkMode(Mode.SingleShotTime)
    public SnapshotLinkedList<Integer> linked() {
        int id = list1.size();
		list1.add(new Snapshot<>(id, id * 10));
        return list1;

    }
    
    @Benchmark
    @Warmup(iterations = WARMUP, batchSize = COUNT)
    @Measurement(iterations = ITERS, batchSize = COUNT)
    @BenchmarkMode(Mode.SingleShotTime)
    public SnapshotArrayList<Integer> array() {
        int id = list2.size();
		list2.add(new Snapshot<>(id, id * 10));
        return list2;
    }

}
