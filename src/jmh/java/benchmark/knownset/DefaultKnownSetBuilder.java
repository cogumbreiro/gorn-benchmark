package benchmark.knownset;

import gorn.sif.KnownSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultKnownSetBuilder<T> implements KnownSetBuilder<T> {
	private final Map<T, KnownSet<T>> known;
	private final List<T> livingTasks;

	public DefaultKnownSetBuilder() {
		this.livingTasks = new ArrayList<>();
		this.known = new HashMap<>();
	}
	
	@Override
	public void initTask(T task, KnownSet<T> root) {
		livingTasks.add(task);
		known.put(livingTasks.get(0), root);
	}
	
	@Override
	public void join(T source, T target) {
		KnownSet<T> k = known.get(source);
		if (!k.tryJoin(target, known.get(target))) {
			throw new IllegalArgumentException();
		}
		livingTasks.remove(target);
	}

	@Override
	public void fork(T source, T newTask) {
		KnownSet<T> k = known.get(source);
		livingTasks.add(newTask);
		KnownSet<T> newKnownSet = k.clone();
		k.fork(newTask);
		known.put(newTask, newKnownSet);
	}

	@Override
	public KnownSet<T> getKnownSet(T task) {
		return known.get(task);
	}
	
	@Override
	public List<T> getRunningTasks() {
		return Collections.unmodifiableList(livingTasks);
	}
}
