package benchmark.knownset;

import gorn.sif.BasicKnownSet;
import gorn.sif.CachedKnownSet;
import gorn.sif.SnapKnownSet;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

import benchmark.knownset.KnownSetGenerator.Strategy;

public class Harness {
	public static <T> T nextGet(Random rnd, List<T> lst) {
		return lst.get(rnd.nextInt(lst.size()));
	}

	public final Map<Strategy, KnownSetBuilder<Task>> gen;

	public final Task targetTask;
	public final CachedKnownSet<Task> sourceCached;
	public final BasicKnownSet<Task> sourceUncached;
	public final SnapKnownSet<Task> sourceSnap;
	public final Collection<Task> memberTasks;

	public Harness(int taskCount) {
		Random rnd = new Random(0xdeadb00f);
		gen = KnownSetGenerator.run(rnd, taskCount);
		KnownSetBuilder<Task> tmp = gen.get(Strategy.CACHED);
		targetTask = KnownSetGenerator.nextGet(rnd, tmp.getRunningTasks());
		Task sourceTask = KnownSetGenerator.nextGet(rnd, tmp.getRunningTasks());
		sourceCached = (CachedKnownSet<Task>)  gen.get(Strategy.CACHED).getKnownSet(sourceTask);
		memberTasks = sourceCached.asCollection();
		sourceUncached = (BasicKnownSet<Task>) gen.get(Strategy.UNCACHED).getKnownSet(sourceTask);
		sourceSnap = (SnapKnownSet<Task>) gen.get(Strategy.SNAPSHOT).getKnownSet(sourceTask);
	}

	//public static final Harness SMALL = new Harness(1000);
	//public static final Harness MEDIUM = new Harness(10000);
	public static final Harness LARGE = new Harness(10000);
	//public static final Harness XLARGE = new Harness(1000000);
}
