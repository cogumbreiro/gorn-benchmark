package benchmark.knownset;

import gorn.sif.KnownSet;

import java.util.List;

public interface KnownSetBuilder<T> {

	void initTask(T task, KnownSet<T> root);

	void join(T source, T target);

	void fork(T source, T newTask);

	KnownSet<T> getKnownSet(T task);

	List<T> getRunningTasks();

}