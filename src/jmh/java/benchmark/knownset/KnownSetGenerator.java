package benchmark.knownset;

import gorn.sif.BasicKnownSet;
import gorn.sif.CachedKnownSet;
import gorn.sif.KnownSet;
import gorn.sif.SnapKnownSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class KnownSetGenerator {
	public static <T> T nextGet(Random rnd, List<T> lst) {
		return lst.get(rnd.nextInt(lst.size()));
	}
	
	// ---
	
	private final KnownSetBuilder<Task> builder;
	
	public KnownSetGenerator(KnownSetBuilder<Task> builder) {
		this.builder = builder;
	}
	
	
	public void initTask(Task task, KnownSet<Task> root) {
		this.builder.initTask(task, root);
	}
	
	private KnownSet<Task> get(Task task) {
		return builder.getKnownSet(task);
	}
	
	/**
	 * Performs one random action in the known graph.
	 */
	public void run(Random rnd) {
		Task source = nextTask(rnd);
		if (rnd.nextDouble() > 0.1) {
			doSpawn(source);
		} else {
			if (! doJoin(rnd, source) ) {
				doSpawn(source);
			}
		}
	}
	
	public static enum Strategy {
		CACHED,
		UNCACHED,
		SNAPSHOT
	}
		
	public Task nextTask(Random rnd) {
		return nextGet(rnd, builder.getRunningTasks());
	}
	
	private void doSpawn(Task source) {
		Task newTask = new Task();
		builder.fork(source, newTask);
	}

	private boolean doJoin(Random rnd, Task source) {
		ArrayList<Task> taskToJoin = new ArrayList<Task>(get(source).asCollection());
		if (taskToJoin.isEmpty()) {
			return false;
		}
		Task target = nextGet(rnd, taskToJoin);
		builder.join(source, target);
		return true;
	}

	private static KnownSet<Task> create(Strategy s) {
		switch(s) {
		case CACHED:
			return new CachedKnownSet<Task>(256);
		case UNCACHED:
			return new BasicKnownSet<Task>();
		case SNAPSHOT:
			return new SnapKnownSet<Task>();
		}
		throw new IllegalStateException();
	}
	
	public static Map<Strategy, KnownSetBuilder<Task>> run(Random rnd, int steps) {
		// Initialize builders
		Map<Strategy,KnownSetBuilder<Task>> result = new HashMap<>();
		Task first = new Task();
		for (Strategy s : Strategy.values()) {
			DefaultKnownSetBuilder<Task> builder = new DefaultKnownSetBuilder<Task>();
			builder.initTask(first, create(s));
			result.put(s, builder);
		}
		ReplicatedKnownSetBuilder<Task> builder = new ReplicatedKnownSetBuilder<Task>(result.values());
		// run the code
		KnownSetGenerator gen = new KnownSetGenerator(builder);
		for (int i = 0; i < steps; i++) {
			gen.run(rnd);
		}
		return result;
	}
}
