package benchmark.knownset;

import gorn.sif.KnownSet;

import java.util.List;

public class ReplicatedKnownSetBuilder<T> implements KnownSetBuilder<T> {
	private final Iterable<KnownSetBuilder<T>> builders;

	public ReplicatedKnownSetBuilder(Iterable<KnownSetBuilder<T>> builders) {
		this.builders = builders;
	}

	@Override
	public void initTask(T task, KnownSet<T> root) {
		throw new IllegalStateException();
	}

	@Override
	public void join(T source, T target) {
		for (KnownSetBuilder<T> builder : builders) {
			builder.join(source, target);
		}
	}

	@Override
	public void fork(T source, T newTask) {
		for (KnownSetBuilder<T> builder : builders) {
			builder.fork(source, newTask);
		}
	}

	@Override
	public KnownSet<T> getKnownSet(T task) {
		for (KnownSetBuilder<T> builder : builders) {
			return builder.getKnownSet(task);
		}
		return null;
	}

	@Override
	public List<T> getRunningTasks() {
		for (KnownSetBuilder<T> builder : builders) {
			return builder.getRunningTasks();
		}
		return null;
	}
}
